$(function(){
/* BASE */

	/* FECHA FOOTER */
	var fecha = new Date();
	var ano =fecha.getFullYear();
	$('#id_year').text(ano)

	/* MENU RESPONSIVE */
	//se clona 'menu-list' para poder tener mas flexibilidad y control
	$('.headerMenu').clone().appendTo('.menu-wrapper').removeClass('menu');
	$('.headerFirst').clone().appendTo('.menu-wrapper').removeClass('menu');

	

	$('.menu-mobile-open').click(function(){
		$(this).addClass('active');
		$('.menu-mobile-close').addClass('active');
		$('.menu-sidebar').addClass('active');
		$('.menu-overlay').addClass('active');
		$('.cnt-wrapper').addClass('active');
		$('.footer').addClass('active');
		$('body').addClass('active');
	});

	$('.menu-link.programas').click(function(){
		$('.menu-link.programas .submenu-programas').toggleClass('active');
	});

	$('.menu-link.noticias').click(function(){
		$('.menu-link.noticias .submenu-programas').toggleClass('active');
	});

	// funcion  para cerrar menu responsive
	function cerrar_nav() {
		$('.menu-sidebar').removeClass('active');
		$('.menu-overlay').removeClass('active');
		$('.menu-mobile-close').removeClass('active');
		$('.menu-mobile-open').removeClass('active');
		$('.cnt-wrapper').removeClass('active');
		$('.footer').removeClass('active');
		$('body').removeClass('active');
	};

	//click en boton cerrar y overlay
	$('.menu-mobile-close').click(function() {
		cerrar_nav();
	});

	$('.menu-overlay').click(function() {
		cerrar_nav();
	});


	//para cerrar el menu responsive en caso hagan resize, o giren la tablet o celular con el menu responsive abierto
		//detectando moviendo de ipad y tablet
	function readDeviceOrientation() {
	    switch (window.orientation) {
	    case 0:
	        break;
	    case 180:
	        break;
	    case -90:
	        break;
	    case 90:
	        break;
	    }
	}
	//detectando tablet, celular o ipad
	dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))

	if ( dispositivo_movil ) {
			function readDeviceOrientation() {
			    if (Math.abs(window.orientation) === 90) {
			        // Landscape
			      	cerrar_nav();
			    } else {
			    	// Portrait
			    	cerrar_nav();
			    }
			}
			window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function(event) {
			var estadomenu = $('.menu-responsive').width();
			if(estadomenu != 0){
				cerrar_nav();
			}
		});
	};
	//Detectando navegador
		$.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());

		/* Detect Chrome */
		if($.browser.chrome){
			/* Do something for Chrome at this point */
			// alert("You are using Chrome!");
			
			/* Finally, if it is Chrome then jQuery thinks it's 
			   Safari so we have to tell it isn't */
			$.browser.safari = false;
		}

		/* Detect Safari */
		if($.browser.safari){
			/* Do something for Safari */
			// alert("You are using Safari!");
		}






	// Ancla scroll - AGREGAR CLASE DEL ENLACE
	$('miclase[href*=#]').click(function() {
	if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
			var $target = $(this.hash);
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if ($target.length) {
			var targetOffset = $target.offset().top;
			$('html,body').animate({scrollTop: targetOffset}, 1000);
			return false;
			}
		}
	});

	// Reseteando cajas de texto administrables
	$('.no-style *').removeAttr('style');


	// Menu responsive traslucido con scrolling
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		console.log(altoScroll)
		if (altoScroll > 60) {
			$('.menu-mobile-open').addClass('scrolling');
			$('.header-ctn').addClass('active');
		}else{
			$('.menu-mobile-open').removeClass('scrolling');
			$('.header-ctn').removeClass('active');
		};
		if (altoScroll > 300) {
			$('.header2').addClass('scrolling');
		}else{
			$('.header2').removeClass('scrolling');
		};
		if (altoScroll > 115) {
			$('.header').addClass('active');
		}else{
			$('.header').removeClass('active');
		};
	});


	// controlar los placeholde
	$('input, textarea').placeholder();

/* --- FIN BASE --- */

	$('.buscar-lupa').click(function(e) {
		e.preventDefault();

		$('.menu-buscador').toggleClass('active');
		$('.buscador').toggleClass('active');
		//$(this).toggleClass('active');
		$('#buscador').focus();

	});

	$('.menu-mobile-open').click(function(e) {
		e.preventDefault();

		$('.menu-buscador').addClass('active');
		$('.buscador').addClass('active');
		$('#buscador').focus();

	});

	$('.menu-mobile-close').click(function(e) {
		e.preventDefault();

		$('.menu-buscador').removeClass('active');
		$('.buscador').removeClass('active');

	});

	//calcular alto
	var height = $(window).height();

	$('.header1-menu1-burguerBox').height(height);

	$('.header1-menu2-hamburguesa').click(function(e) {
		e.preventDefault();

		$('.header1-menu1-burguerOpen').toggleClass('active');
	});

	$('.header2-right-boton').click(function(e) {
		e.preventDefault();

		$('.header1-menu1-burguerOpen').toggleClass('active');
	});

	$('.header1-menu2-icon').click(function(e) {
		e.preventDefault();

		$('.header1-menu1-burguerOpen').removeClass('active');
	});

});
